<?php

namespace Pinvds;

use Illuminate\Remote\Connection;

class Vds
{
    /**
     * Используется для отслеживания возврата запросов от удаленного сервера.
     */
    const TIMEOUT_MARKER = '=== timeout ===';

    /**
     * Объект удаленного соединения
     *
     * @var null|Connection
     */
    protected $ssh = null;

    /**
     * Vds constructor.
     *
     * @param Connection $ssh Объект удаленного соединения
     */
    public function __construct(Connection $ssh)
    {
        $this->ssh = $ssh;
    }

    /**
     * Выполняет команду, или набор команд на удаленном сервере
     *
     * @param string|array $command Команда (команды)
     * @param integer      $timeout Максимальное время ожидания ответа
     *
     * @return string
     */
    public function runCommand($command, $timeout = 100)
    {
        $result = self::TIMEOUT_MARKER;

        $this->ssh->run($command, function($line) use (&$result)  {
            $result = $line;
        });

        $cnt = 0;
        while (true) {
            if ($cnt > $timeout) {
                break;
            }

            sleep(1);
            if (self::TIMEOUT_MARKER !== $result) {
                return preg_replace('~^(.+)\s$~', '$1', $result);
            }
            $cnt++;
        }

        return $result;
    }

    /**
     * Создает новую виртуальную машину
     *
     * @param string $discPath   Путь к диску виртуальной машины, например /dev/thinpool/test_1
     *                           Здесь - test_1 - это имя диска и имя создаваемой машины.
     * @param integer $size      Размер диска ВМ (в гигабайтах)
     * @param integer $ram       Размер оперативной памяти (в мегабайтах)
     * @param integer $cntCpu    Количество ядер процессора.
     * @param string  $listenIP  IP адрес.
     * @param string  $password  Пароль.
     * @param integer $port      Номер порта.
     * @param string  $osType    Тип ОС.
     * @param string  $osVariant Вариант ОС.
     * @param string  $cdPath    Путь к файлу с установочным образом диска (iso).
     *
     * @return boolean
     *
     * @throws \Exception
     */
    public function createVm($discPath, $size, $ram, $cntCpu, $listenIP, $password, $port, $osType, $osVariant, $cdPath)
    {
        $size   = (int)$size;
        $ram    = (int)$ram;
        $cntCpu = (int)$cntCpu;
        $port   = (int)$port;

        $vmName = preg_replace('~^.+/([^/]+)$~', '$1', $discPath);

        // проверка существования виртуальной машины
        if( $this->checkExistsVm($vmName) ) {
            throw new \Exception("VM {$discPath} already exists");
        }
        
        // проверка существования директории
        $discDir = preg_replace('~^(.+)/[^/]+$~', '$1', $discPath);
        $result = $this->runCommand('[ -d "' . $discDir . '" ] && echo 1 || echo 0');
        
        if (1 != $result) {
            throw new \Exception("Dir {$discDir} not exists");
        }

        // проверка наличия установочного диска
        $result = $this->runCommand('[ -f "' . $cdPath . '" ] && echo 1 || echo 0');
        if (1 != $result) {
            throw new \Exception("File {$cdPath} not exists");
        }

        // имя пула
        $poolName = preg_replace('~^.+/([^/]+)$~', '$1', $discDir);

        // создаем диск
        $this->runCommand('lvcreate --thinpool lvol1 ' . $poolName . ' -n ' . $vmName . ' -V' . $size . 'G');

        // создаем машину
        $result = $this->runCommand('virt-install  --connect qemu:///system --name ' . $vmName . ' --ram ' . $ram . ' --vcpus ' . $cntCpu . '  --disk path=' . $discPath . '  --network=bridge:br0,   --graphics=vnc,listen=' . $listenIP . ',password=' . $password . ',port=' . $port . '  --os-type=' . $osType . ' --os-variant=' . $osVariant . ' --cdrom ' . $cdPath . ' --noautoconsole');

        if (! preg_match('~Domain installation still in progress~', $result)) {
            throw new \Exception("Error creating VM. Message: {$result}");
        }

        return true;
    }

    /**
     * Создание новой виртуальной машины из шаблона
     *
     * @param string $discPath        Путь к диску виртуальной машины, например /dev/thinpool/test_1
     *                                Здесь - test_1 - это имя диска и имя создаваемой машины.
     * @param integer $size           Размер диска ВМ (в гигабайтах).
     * @param string  $mac            Мак-адрес новой машины.
     * @param integer $port           Порт.
     * @param string  $vmTemplateName Имя машины - шаблона.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function createVmWithTemplate($discPath, $size, $mac, $port, $vmTemplateName)
    {
        $size   = (int)$size;
        $port   = (int)$port;

        $vmName = preg_replace('~^.+/([^/]+)$~', '$1', $discPath);

        // проверка существования виртуальной машины
        if( $this->checkExistsVm($vmName) ) {
            throw new \Exception("VM {$discPath} already exists");
        }

        // проверка существования машины - шаблона
        if( ! $this->checkExistsVm($vmTemplateName) ) {
            throw new \Exception("VM {$vmTemplateName} not exists");
        }

        // проверка существования директории
        $discDir = preg_replace('~^(.+)/[^/]+$~', '$1', $discPath);
        $result = $this->runCommand('[ -d "' . $discDir . '" ] && echo 1 || echo 0');

        if (1 != $result) {
            throw new \Exception("Dir {$discDir} not exists");
        }

        // имя пула
        $poolName = preg_replace('~^.+/([^/]+)$~', '$1', $discDir);

        // создаем диск
        $this->runCommand('lvcreate --thinpool lvol1 ' . $poolName . ' -n ' . $vmName . ' -V' . $size . 'G');

        // клонируем машину
        $this->runCommand("virt-clone -o {$vmTemplateName} -n {$vmName} -file {$discPath} -mac {$mac}");

        $this->runCommand("virt-sysprep -d {$vmName}");

        $this->setPort($vmName, $port);

        $this->runCommand("virsh autostart {$vmName}");
    }

    /**
     * Установка нового порта для машины
     *
     * @param string $vmName Имя виртуальной машины.
     * @param integer $port  Новый номер порта.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function setPort($vmName, $port)
    {
        $port   = (int)$port;
        $filePath = '/etc/libvirt/qemu/' . $vmName . '.xml';

        $xml = $this->ssh->getString($filePath);
        if (!$xml) {
            throw new \Exception("File {$filePath} not found");
        }

        $xml = preg_replace('~^(.+<graphics[^>]+port=)\'[-]{0,1}\d+(\'.+)$~s', '$1\'' . $port . '$2', $xml);
        $xml = preg_replace('~^(.+<graphics[^>]+autoport=\')[^\']+(\'.+)$~s', '$1no$2', $xml);

        $this->ssh->putString($filePath, $xml);
        $this->runCommand('virsh define ' . $filePath);
    }

    /**
     * Проверка существования виртуальной машины.
     *
     * @param string $vmName Имя виртуальной машины. Только имя, без полного пути к диску, например test_1
     *
     * @return boolean
     */
    public function checkExistsVm($vmName)
    {
        return (bool) preg_match('~\s' . $vmName . '\s~', $this->runCommand('virsh list --all'));
    }

    /**
     * Стандартная перезагрузка.
     *
     * @param string $vmName Имя машины.
     *
     * @return void
     */
    public function vmReboot($vmName)
    {
        $this->runCommand('virsh reboot ' . $vmName);
    }

    /**
     * Принудительная перезагрузка.
     *
     * @param string $vmName Имя машины.
     *
     * @return void
     */
    public function vmForceReboot($vmName)
    {
        $this->runCommand('virsh destroy ' . $vmName);
        $this->runCommand('virsh start ' . $vmName);
    }

    /**
     * Стандартная остановка.
     *
     * @param string $vmName Имя машины.
     *
     * @return void
     */
    public function vmShutdown($vmName)
    {
        $this->runCommand('virsh shutdown ' . $vmName);
    }

    /**
     * Принудительная остановка.
     *
     * @param string $vmName Имя машины.
     *
     * @return void
     */
    public function vmForceShutdown($vmName)
    {
        $this->runCommand('virsh destroy ' . $vmName);
    }

    /**
     * Запуск виртуальной машины.
     *
     * @param string $vmName Имя машины.
     *
     * @return void
     */
    public function vmStart($vmName)
    {
        $this->runCommand('virsh start ' . $vmName);
    }

    /**
     * Установка максимальной памяти для машины.
     *
     * @param string  $vmName Имя виртуальной машины.
     * @param integer $ram    Размер оперативной памяти (в мегабайтах).
     *
     * @return void
     */
    public function setMaxMem($vmName, $ram)
    {
        $ram = (int)$ram;
        $this->runCommand("virsh setmaxmem {$vmName} {$ram}M --config");
    }

    /**
     * Установка текущей доступной памяти для машины.
     *
     * @param string  $vmName Имя виртуальной машины.
     * @param integer $ram    Размер оперативной памяти (в мегабайтах).
     *
     * @return void
     */
    public function setMem($vmName, $ram)
    {
        $ram = (int)$ram;
        $this->runCommand("virsh setmem {$vmName} {$ram}M --config");
    }

    /**
     * Смена пароля root.
     * ВНИМАНИЕ!!! Перед сменой пароля машину нужно ВЫКЛЮЧИТЬ.
     * После смены - включить.
     *
     * @param string $vmName   Имя виртуальной машины.
     * @param string $password Новый пароль root.
     *
     * @return void
     */
    public function changeRootPassword($vmName, $password)
    {
        $this->runCommand("virt-sysprep --root-password password:{$password} -d {$vmName}");
    }

    /**
     * Установка максимального количества ядер процессора.
     * ВНИМАНИЕ!!! После выполнения данной команды - нужно выключить и опять включить машину.
     *
     * @param string  $vmName Имя виртуальной машины.
     * @param integer $cpu    Устанавливаемое количество ядер.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function setMaxCpu($vmName, $cpu)
    {
        $cpu = (int)$cpu;
        $filePath = '/etc/libvirt/qemu/' . $vmName . '.xml';

        $xml = $this->ssh->getString($filePath);
        if (!$xml) {
            throw new \Exception("File {$filePath} not found");
        }

        $xml = preg_replace('~(.+)>\d+(<\/vcpu\>.+)$~s', '$1>' . $cpu . '$2', $xml);

        $this->ssh->putString($filePath, $xml);
        $this->runCommand('virsh define ' . $filePath);
    }

    /**
     * Установка текущего количества ядер.
     * ВНИМАНИЕ!!! Текущее количество не может быть больше максимального.
     * Максимальное устанавливается в методе setMaxCpu.
     *
     * @param string  $vmName Имя виртуальной машины.
     * @param integer $cpu    Устанавливаемое количество ядер.
     *
     * @return void
     */
    public function setCurrentCpu($vmName, $cpu)
    {
        $cpu = (int)$cpu;
        $this->runCommand("virsh setvcpus {$vmName} {$cpu} --live --config");
    }

    /**
     * Реинсталл системы из шаблона.
     *
     * @param string $discPath       Путь к диску виртуальной машины, например /dev/thinpool/test_1
     *                               Здесь - test_1 - это имя диска и имя виртуальной машины.
     * @param string $vmTemplateName Имя машины - шаблона.
     * @param string $mac            Мак-адрес машины.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function reinstallWithTemplate($discPath, $vmTemplateName, $mac)
    {
        $vmName = preg_replace('~^.+/([^/]+)$~', '$1', $discPath);
        $configPath = '/etc/libvirt/qemu/' . $vmName . '.xml';

        // проверка существования виртуальной машины
        if( ! $this->checkExistsVm($vmName) ) {
            throw new \Exception("VM {$discPath} not exists");
        }

        // проверка существования машины - шаблона
        if( ! $this->checkExistsVm($vmTemplateName) ) {
            throw new \Exception("VM {$vmTemplateName} not exists");
        }

        // останавливаем машину
        $this->runCommand("virsh destroy {$vmName}");

        // копия конфига
        $this->runCommand("cp {$configPath} {$configPath}.bak");

        // удаляем машину
        $this->deleteVm($vmName);

        // клонируем машину
        $this->runCommand("virt-clone -o {$vmTemplateName} -n {$vmName} -file {$discPath} -mac {$mac}");

        $this->runCommand("virt-sysprep -d {$vmName}");

        // возвращаем конфиг
        $this->runCommand("mv {$configPath}.bak {$configPath}");

        $this->runCommand("virsh define {$configPath}");
        $this->runCommand("virsh autostart {$vmName}");
        $this->runCommand("virsh start {$vmName}");
    }

    /**
     * Удаляет виртуальную машину
     *
     * @param string $vmName Имя виртуальной машины.
     *
     * @return void
     */
    public function deleteVm($vmName)
    {
        $this->runCommand("virsh undefine {$vmName}");
    }

    /**
     * Получение разделов жесткого диска.
     *
     * @param string $discPath Путь к диску виртуальной машины. (Например /dev/thinpool/test_1).
     *
     * @return string
     */
    public function getSections($discPath)
    {
        return $this->runCommand("virt-filesystems -h --long -a  {$discPath}");
    }

    /**
     * Ресайз жесткого диска.
     *
     * @param string $discPath     Путь к диску виртуальной машины, например /dev/thinpool/test_1
     *                             Здесь - test_1 - это имя диска и имя виртуальной машины.
     * @param integer $size        Новый размер жесткого диска в гигайбатах.
     * @param string  $sectionName Имя раздела жесткого диска для изменения.
     *
     * @return string
     *
     * @throws \Exception
     */
    public function resizeDisc($discPath, $size, $sectionName)
    {
        $size = (int)$size;

        $vmName = preg_replace('~^.+/([^/]+)$~', '$1', $discPath);
        $discDir = preg_replace('~^(.+)/[^/]+$~', '$1', $discPath);

        // проверка существования виртуальной машины
        if( ! $this->checkExistsVm($vmName) ) {
            throw new \Exception("VM {$discPath} not exists");
        }

        $this->runCommand("lvrename {$discPath} {$discPath}.bak");
        $this->runCommand("lvcreate -L {$size}G -n {$vmName} {$discDir}");
        return $this->runCommand("virt-resize --expand {$sectionName} {$discPath}.bak {$discPath}");
    }
}
