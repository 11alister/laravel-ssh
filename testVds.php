<?php

require_once __DIR__ . '/Vds.php';
require_once __DIR__ . '/vendor/autoload.php';

use Illuminate\Remote\Connection;
use Pinvds\Vds;

// конфиг
require_once __DIR__ . '/config/vds.php';

// соединение с сервером.
$ssh = new Connection('vds', $vdsConfig['host'], $vdsConfig['user'], array('password' => $vdsConfig['password']));

$vds = new Vds($ssh);
// создание машины
//$vds->createVm('/dev/thinpool/gleb_2', 8, 1024, 2, '0.0.0.0', 'foobar', 59333, 'linux', 'debianwheezy', '/tmp/ubuntu.iso');

// проверка существования машины
//var_dump($vds->checkExistsVm('test_4'));

//$vds->setMaxCpu('gleb_1', 4);

$vds->setPort('gleb_2', 53458);
