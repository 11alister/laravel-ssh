<?php

// autoload_namespaces.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'System' => array($vendorDir . '/phpseclib/phpseclib/phpseclib'),
    'Symfony\\Component\\Finder\\' => array($vendorDir . '/symfony/finder'),
    'Net' => array($vendorDir . '/phpseclib/phpseclib/phpseclib'),
    'Math' => array($vendorDir . '/phpseclib/phpseclib/phpseclib'),
    'Illuminate\\Support' => array($vendorDir . '/illuminate/support'),
    'Illuminate\\Remote' => array($vendorDir . '/illuminate/remote'),
    'Illuminate\\Filesystem' => array($vendorDir . '/illuminate/filesystem'),
    'File' => array($vendorDir . '/phpseclib/phpseclib/phpseclib'),
    'Crypt' => array($vendorDir . '/phpseclib/phpseclib/phpseclib'),
);
